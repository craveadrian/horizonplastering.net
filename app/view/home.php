<div id="content">
	<div id="welcome-section">
		<div class="row">
			<h1>Horizon Plastering</h1>
			<div class="welcome-section-content">
				<div class="welcome-image">
					<img src="public/images/content/img1.jpg" alt="Plastering Men Image">
				</div>
				<div class="welcome-article">
					<h2>Welcome!</h2>
					<p>Horizon Plastering has been in business since 2012 with more than 30 years experience servicing Los Angeles County, Orange County, Riverside County and San Bernardino County. Here at Horizon Plastering we take pride in what we do and treat all of your needs as if they were our own. No project is too big or too small for us. Give us a call for a free estimate. We do outside plaster only.</p>
					<a href="<?php echo URL ?>services#content" class="btn">LEARN MORE</a>
				</div>
			</div>
		</div>
	</div>
	<div id="services-section">
		<div class="row">
			<h2>Our Services</h2>
			<div class="services">
				<div class="box left">
					<div class="image"> <img src="public/images/content/services1.jpg" alt="RESIDENTIAL PLASTER INSTALL / REPAIR IMAGE"> </div>
					<div class="title">
						<img src="public/images/content/overlay.png" alt="overlay">
						<a href="<?php echo URL ?>services#content"><p>RESIDENTIAL PLASTER INSTALL / REPAIR</p></a>
					</div>
				</div>
				<div class="box right">
					<div class="image"> <img src="public/images/content/services2.jpg" alt="COMMERCIAL PLASTER INSTALL / REPAIR IMAGE"> </div>
					<div class="title">
						<img src="public/images/content/overlay.png" alt="overlay">
						<a href="<?php echo URL ?>services#content"><p>COMMERCIAL PLASTER INSTALL / REPAIR</p></a>
					</div>
				</div>
				<p class="top">PROUDLY SERVING RIVERSIDE, CA AND THE SURROUNDING AREA SINCE 2012</p>
				<p>Horizon Plastering offers high quality work performed by qualified professionals</p>
			</div>
		</div>
	</div>
	<div id="contact-section">
		<div class="row">
			<h2>Contact Us</h2>
			<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name:">
				</label>
				<label><span class="ctc-hide">Phone</span>
					<input type="text" name="phone" placeholder="Phone:">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email:">
				</label>
				<label><span class="ctc-hide">Message</span>
					<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
				</label>
				<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>
				<label>
					<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</label>
				<?php endif ?>
				<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
			</form>
		</div>
	</div>
	<div id="review-section">
		<div class="row">
			<div class="blockquote">
				<h2>Reviews</h2>
				<img src="public/images/content/bquote.png" alt="Block Quote">
				<p>I was called 5 minutes after the owner Jose Torres was contacted by Home Advisor! He came to my home that day and we agreed on the work and price. He started the job 2 days later and completed work the day after that. This job was a code-required fire wall to replace a window between garage and kitchen. This demanded a rough-stucco surface in the garage and a smooth plaster with texture on the interior wall. He did an outstanding job on both and my realtor wants to use him. I would highly recommend you consider him for your job!”</p>
			</div>
			<div class="image">
				<img src="public/images/content/img2.jpg" alt="Royal Sala">
			</div>
		</div>
	</div>
	<div id="contact-description-section">
		<div class="row">
			<h2>Get intouch with us</h2>
			<p>We will be glad to answer your questions, feel free to ask a piece of information or a quotation.
				<span>We are looking forward to work with you.</span></p>
			<div class="logo2">
				<a href="<?php echo URL ?>" ><img src="public/images/content/logo2.png" alt="Horizaon Plastering Logo 2"></a>
			</div>
			<div class="contact-description">
				<div class="cdLeft">
					<img src="public/images/content/email.png" alt="Email Icon">
					<p class="email"><?php $this->info(["email","mailto"]); ?></p>
				</div>
				<div class="cdMid">
					<img src="public/images/content/phone.png" alt="Phone Icon">
					<p class="phone"><?php $this->info(["phone","tel"]); ?></p>
				</div>
				<div class="cdRight">
					<img src="public/images/content/email.png" alt="Email Icon">
					<p class="location"> <a href="#">Put Address Here, <span>Put Address Here</span></a></p>
				</div>
			</div>
		</div>
	</div>
</div>
